{include file='../common/header.tpl'}
{foreach from=$ptuts key=k item=it}	
<form style="display: none;" id="remove_form{$it->ptu_num}" role="form" method="post" action="/ptut/ptuts/remove">
	<input type="hidden" name="ptut_num" value="{$it->ptu_num}"> 
</form>
<form style="display: none;" id="edit_form{$it->ptu_num}" role="form" method="post" action="/ptut/ptuts/edit">
	<input type="hidden" name="ptut_num" value="{$it->ptu_num}"> 
</form>
<form style="display: none;" id="addsupervisor_form{$it->ptu_num}" role="form" method="post" action="/ptut/supervisors/add">
	<input type="hidden" name="ptut_num" value="{$it->ptu_num}"> 
</form>
<form style="display: none;" id="removesupervisor_form{$it->ptu_num}" role="form" method="post" action="/ptut/supervisors/remove">
	<input type="hidden" name="ptut_num" value="{$it->ptu_num}"> 
</form>
{/foreach}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gestion des Projets Tutorés
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th class="text-center">Nom</th>
						<th class="text-center">Numéro</th>					
						<th class="text-center">Description</th>
						<th class="text-center">Date de création</th>
						<th class="text-center">Dossier</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody>	
					{foreach from=$ptuts key=k item=it}						
					<tr>
						<td class="text-center">
							{$it->ptu_nom}</a>
						</td>
						<td class="text-center">
							{$it->ptu_num}
						</td>					
						<td class="text-center">
							{$it->ptu_description}
						</td>
						<td class="text-center">
							{$it->ptu_date_creation}
						</td>
						<td class="text-center">
							{$it->ptu_dossier_racine}
						</td>
						<td class="text-center">
	    					<div class="btn-group">
	    						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form{$it->ptu_num}.submit();">
                                    	<a href="#">Editer</a>
                                    </li>
                                    <li onclick="addsupervisor_form{$it->ptu_num}.submit();">
                                    	<a href="#">Ajouter un encadrent</a>
                                    </li>
                                    <li onclick="removesupervisor_form{$it->ptu_num}.submit();">
                                    	<a href="#">Supprimer un encadrent</a>
                                    </li>
                                    <li onclick="remove_form{$it->ptu_num}.submit();">
                                    	<a href="#"><span class="glyphicon glyphicon-alert"></span> Supprimer</a>
                                    </li>
                                </ul>
	    					</div>
						</td>					
					</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
    	<div class="col-lg-12">
			<h3>Ajouter un Projet Tutoré</h3>

			<form role="form" method="post" action="/ptut/ptuts/add">
				<div class="form-group">
					<label>Nom du projet</label>
					<input name="name" type="text"  placeholder="Nom*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label>Description</label>
					<input name="description" type="text"  placeholder="Description*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label>Nom dossier</label>
					<input name="directory" type="text"  placeholder="Dossier Racine*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<div class="btn-group" role="group" aria-label="...">
						<button type="submit" class="btn btn-success">Ajouter</button>
					</div>
				</div>
			</form>
    	</div>
    </div>
{include file='../common/footer.tpl'}