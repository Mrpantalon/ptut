<div class="pull-right">
	    					<div class="btn-group">
	    						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                    	<a href="#">Editer le PTUT</a>
                                    </li>
                                    <li>
                                    	<a href="#">Ajouter un encadrent</a>
                                    </li>
                                    <li>
                                    	<a href="#">Supprimer un encadrent</a>
                                    </li>
                                </ul>
	    					</div>
	    				</div>

{include file='../common/header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mon Projet Tutoré
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		{if $ptut|@count>=1}
	    		{foreach from=$ptut key=k item=it}
	    		{if $ptut|@count>=2}
		    		<div class="panel panel-default">
		    			<div class="panel-heading">
		    				<h3 class="panel-title">Projet tutoré: {$it->ptu_nom}</h3>
		    			</div>
		    			<div class="panel-body">
	    		{/if}
		    			<div class="table-responsive">	    					
					    	<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
									{if $ptut|@count === 1}
										<th class="text-center">Nom</th>
									{/if}
										<th class="text-center">Numéro</th>					
										<th class="text-center">Description</th>
										<th class="text-center">Date de création</th>
										<th class="text-center">Dossier racine</th>
									</tr>
								</thead>
								<tbody>								
									<tr>
									{if $ptut|@count === 1}
										<td class="text-center">{$it->ptu_nom}</a></td>
									{/if}
										<td class="text-center">{$it->ptu_num}</td>					
										<td class="text-center">{$it->ptu_description}</td>
										<td class="text-center">{$it->ptu_date_creation}</td>
										<td class="text-center">{$it->ptu_dossier_racine}</td>
									</tr>
								</tbody>
							</table>

							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Nom</th>
										<th class="text-center">Prénom</th>
										<th class="text-center">Mail</th>
										<th class="text-center">Statut</th>
										<th class="text-center">Login</th>									
										<th class="text-center">Chef de projet</th>
										<th class="text-center">Etudiant</th>
									</tr>
								</thead>
								<tbody>
									{foreach from=$members[$it->ptu_num] key=k item=it2}
									<tr>									
										<td td class="text-center">{$it2->mem_id}</td>
										<td td class="text-center">{$it2->mem_nom}</td>
										<td td class="text-center">{$it2->mem_prenom}</td>
										<td td class="text-center">{$it2->mem_mail}</td>
										<td td class="text-center">{$it2->mem_statut}</td>
										<td td class="text-center">{$it2->mem_login}</td>									
										<td class="text-center">
										{if $it2->mem_chef_projet == 0}
											non
										{else}
											oui
										{/if}
										</td>
										<td class="text-center">
										{if $it2->mem_etudiant == 0}
											non
										{else}
											oui
										{/if}
										</td>								
									</tr>
								{/foreach}
									</tbody>
							</table>	
						</div>									
					{if $ptut|@count>=2}
	                </div>
	            </div>
				{/if}
			{/foreach}						
			{else}
				<p>Vous n'êtes dans aucun PTUT !</p>
			{/if}
    	</div>
    </div>
{include file='../common/footer.tpl'}