{include file='../common/header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un Projet Tutoré
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		<form role="form" method='post'>
                <input name="apply" type="hidden" value="1">
                <input name="ptut_num" type="hidden" value="{$ptut->ptu_num}">
                <div class="form-group">
                    <label>Nom</label>
                    <input name="name" value="{$ptut->ptu_nom}" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Description</label>  
                    <input name="description" value="{$ptut->ptu_description}" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Nom du dossier</label>  
                    <input name="directory" value="{$ptut->ptu_dossier_racine}" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                        <a href="/ptut/ptuts/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>			
    	</div>
    </div>
{include file='../common/footer.tpl'}