{include file='../common/header.tpl'}
{foreach from=$members_group item=it}
<form style="display: none;" id="remove_form{$it->mem_id}" role="form" method="post">
    <input type="hidden" name="member_id" value="{$it->mem_id}"> 
    <input type="hidden" name="remove_member" value="1"> 
    <input name="gro_num" type="hidden" value="{$gro_num}">
    <input name="group_name" type="hidden" value="{$group_name}">
    <input name="ptu_num" type="hidden" value="{$ptu_num}">
</form>
{/foreach}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un groupe
            </h1>
        </div>
    </div>	
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method='post'>
                <input name="apply" type="hidden" value="1">
                <input name="group_num" type="hidden" value="{$gro_num}">
                <div class="form-group">
                    <label>Nom du groupe</label>
                    <input name="group_name" placeholder="Nom*" value="{$group_name}" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Projet tutoré</label>  
                    <select name="ptu_num" required="required" class="form-control">
                        {foreach from=$ptuts item=it}
                            <option value="{$it->ptu_num}" 
                            {if $it->ptu_num === $ptu_num} 
                            selected 
                            {/if}
                            >{$it->ptu_num} - {$it->ptu_nom}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                        <a href="/ptut/groups/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Prénom</th>
                        <th class="text-center">Mail</th>
                        <th class="text-center">Login</th>  
                        <th class="text-center"></th>                                 
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$members_group key=k item=it2}
                    <tr>                                    
                        <td class="text-center">{$it2->mem_id}</td>
                        <td class="text-center">{$it2->mem_nom}</td>
                        <td class="text-center">{$it2->mem_prenom}</td>
                        <td class="text-center">{$it2->mem_mail}</td>
                        <td class="text-center">{$it2->mem_login}</td> 
                        <td class="text-center">
                            <button type="button" class="btn btn-danger "onclick="remove_form{$it2->mem_id}.submit();">
                                Supprimer
                            </button>
                        </td>                                                   
                    </tr>
                {/foreach}
                    </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method='post'>
                <input name="add_member" type="hidden" value="1">
                <input name="gro_num" type="hidden" value="{$gro_num}">
                <input name="group_name" type="hidden" value="{$group_name}">
                <input name="ptu_num" type="hidden" value="{$ptu_num}">
                <div class="form-group">
                    <label>Ajouté un membre</label>
                    <select name="member_id" required="required" class="form-control">
                        {foreach from=$members item=it}
                            <option value="{$it->mem_id}" >{$it->mem_id} - {$it->mem_nom} - {$it->mem_prenom}</option>
                        {/foreach}
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Ajouter</button>
            </form>
        </div>
    </div>
<br>
{include file='../common/footer.tpl'}