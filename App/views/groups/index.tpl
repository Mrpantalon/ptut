{include file='../common/header.tpl'}
{foreach from=$groups item=it}
<form style="display: none;" id="edit_form{$it->gro_num}" role="form" method="post" action="/ptut/groups/edit">
    <input type="hidden" name="ptu_num" value="{$it->ptu_num}"> 
    <input type="hidden" name="group_name" value="{$it->gro_nom}"> 
    <input type="hidden" name="gro_num" value="{$it->gro_num}"> 
</form>
<form style="display: none;" id="remove_form{$it->gro_num}" role="form" method="post" action="/ptut/groups/remove">
    <input type="hidden" name="id" value="{$it->gro_num}"> 
</form>
{/foreach}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gestion des groupes
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>                
                        <th class="text-center">Nom du groupe</th>
                        <th class="text-center">Numéro du groupe</th>
                        <th class="text-center">Projet (n°/nom)</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$groups item=it}
                    <tr>
                        <td class="text-center">{$it->gro_nom}</td>
                        <td class="text-center">{$it->gro_num}</td>
                        <td class="text-center">{$it->ptu_num} - {$ptuts_grp[$it->ptu_num]->ptu_nom}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form{$it->gro_num}.submit();">
                                        <a href="#">Editer</a>
                                    </li>
                                    <li onclick="remove_form{$it->gro_num}.submit();">
                                        <a href="#"><span class="glyphicon glyphicon-alert"></span> Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>       
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Ajouter un groupe</h3>
            <form role="form" method="post" action="/ptut/groups/add">  
                <div class="form-group">
                    <label>Nom du groupe</label>
                    <input name="group_name" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                <label>Projet tutoré</label>
                    <select name="ptu_num" required="required" class="form-control">
                        {foreach from=$ptuts item=it}
                            <option value="{$it->ptu_num}">{$it->ptu_num} - {$it->ptu_nom}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success" name="addGrp">Ajouter</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
{include file='../common/footer.tpl'}