{include file='../common/header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Ajouter un Encadrent
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">  
            <form role="form" method="post" action="/ptut/supervisors/add"> 
                <input name="apply" type="hidden" value="1">
                <input name="ptut_id" type="hidden" value="{}">              
                <div class="form-group">
                    <label>Membre</label>
                    <select name="memberid" required="required" class="form-control">
                        {foreach from=$members key=k item=it}
                            <option value="{$it->mem_id}">{$it->mem_id} - {$it->mem_nom}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group">
                    <label>Rôle</label>
                    <select name="role" required="required" class="form-control">
                        <option value="commanditaire">commanditaire</option>
                        <option value="tuteur">tuteur</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Ajouter</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>

    	</div>
    </div>
{include file='../common/footer.tpl'}