<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="refresh" content="3; URL=/ptut/connection" />
        <title>Projet tut</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
        <script src="/ptut/Public/js/jquery.min.js"></script>
        <script src="/ptut/Public/js/bootstrap.min.js"></script>
        <script src="/ptut/Public/js/sb-admin-2.min.js"></script>
        <script src="/ptut/Public/js/metisMenu.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Connexion échouée !</h3>
                        </div>
                        <div class="panel-body">
                            <p>La connection a échoué !</p>
                            <p><strong>{$error}</strong></p>
                            <p>Vous allez être redirigé vers la page de connection dans 3 secondes.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
