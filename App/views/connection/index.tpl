<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Projet tut</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
        <script src="/ptut/Public/js/jquery.min.js"></script>
        <script src="/ptut/Public/js/bootstrap.min.js"></script>
        <script src="/ptut/Public/js/sb-admin-2.min.js"></script>
        <script src="/ptut/Public/js/metisMenu.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Connection</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="/ptut/connection/connect">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Identifiant de connection" name="username" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="rememberme">Se souvenir de moi
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-success btn-block">Se connecter</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
