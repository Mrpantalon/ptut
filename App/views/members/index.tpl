{include file='../common/header.tpl'}
    {if isset($err_login) && $err_login == 1}
        <div class="alert alert-danger alert-dismissable" style="margin-top: 70px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Erreur: cet identifiant de connection existe déja !
        </div>
    {/if}
    {foreach from=$members item=it}
        <form style="display: none;" id="edit_form{$it->mem_id}" role="form" method="post" action="/ptut/members/edit">
            <input type="hidden" name="member_id" value="{$it->mem_id}"> 
        </form>
        <form style="display: none;" id="remove_form{$it->mem_id}" role="form" method="post" action="/ptut/members/remove">
            <input type="hidden" name="member_id" value="{$it->mem_id}"> 
        </form>
    {/foreach}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Membres
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Prénom</th>
                        <th class="text-center">Mail</th>
                        <th class="text-center">Statut</th>
                        <th class="text-center">Identifiant de connexion</th>                                  
                        <th class="text-center">Chef de projet</th>
                        <th class="text-center">Etudiant</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$members item=it}
                    <tr>
                        <td td class="text-center">{$it->mem_id}</td>
                        <td td class="text-center">{$it->mem_nom}</td>
                        <td td class="text-center">{$it->mem_prenom}</td>
                        <td td class="text-center">{$it->mem_mail}</td>
                        <td td class="text-center">{$it->mem_statut}</td>
                        <td td class="text-center">{$it->mem_login}</td>                                 
                        <td class="text-center">
                            {if $it->mem_chef_projet == 0}
                                non
                            {else}
                                oui
                            {/if}
                        </td>
                        <td class="text-center">
                            {if $it->mem_etudiant == 0}
                                non
                            {else}
                                oui
                            {/if}
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form{$it->mem_id}.submit();">
                                        <a href="#">Editer</a>
                                    </li>
                                    <li onclick="remove_form{$it->mem_id}.submit();">
                                        <a href="#"><span class="glyphicon glyphicon-alert"></span> Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
    	</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>Ajouter un membre</h3>
            <form role="form" method="post" action="/ptut/members/add">  
                <div class="form-group">
                    <label>Nom</label>
                    <input name="member_name" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Prénom</label>
                    <input name="member_firstname" placeholder="Prénom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" placeholder="Email*" type="mail" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Statut</label>
                    <select class="form-control" name="member_status">
                        <option>enseignant</option>
                        <option>étudiant</option>
                        <option>extérieur</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Identifiant de connexion</label>
                    <input name="member_login" placeholder="Identifiant*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>
                        Chef de projet <input name="member_projet_owner" type="checkbox"  value="1">
                    </label>
                </div>
                <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" placeholder="Mot de passe*" type="password" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Ajouter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
{include file='../common/footer.tpl'}