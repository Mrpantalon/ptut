{include file='../common/header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un membre
            </h1>
        </div>
    </div>	
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method="post" action="/ptut/members/edit">  
                <input type="hidden" name="member_id" value="{$member_id}">
                <input type="hidden" name="apply" value="1">
                <div class="form-group">
                    <label>Nom</label>
                    <input name="member_name" value="{$member_name}" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Prénom</label>
                    <input name="member_firstname" value="{$member_firstname}" placeholder="Prénom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" value="{$member_mail}" placeholder="Email*" type="mail" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Statut</label>
                    <select class="form-control" name="member_status">
                        <option {if $member_status  == "enseignant"} selected="selected"  {/if}>enseignant</option>
                        <option {if $member_status  == "étudiant"} selected="selected"  {/if}>étudiant</option>
                        <option {if $member_status  == "extérieur"} selected="selected"  {/if}>extérieur</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Identifiant de connexion</label>
                    <input name="member_login" value="{$member_login}" placeholder="Identifiant*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>
                        Chef de projet <input name="member_projet_owner" {if $member_projet_owner == 1} checked {/if} type="checkbox" value="1">
                    </label>
                </div>
                <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" placeholder="Mot de passe*" type="password" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button class="btn btn-warning" type='reset'>Réinitialiser le formulaire</button>
                        <a href="/ptut/members/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<br>
{include file='../common/footer.tpl'}