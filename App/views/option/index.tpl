{include file='../common/header.tpl'}
    {if isset($success) && $success == 1}
        <div class="alert alert-success alert-dismissable" style="margin-top: 70px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Les modifications ont bien été prise en compte.
        </div>
    {/if}
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Options de l'utilisateur
            </h1>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">
            <h3>Informations</h3>
            <h4>Nom</h4>
            <p>{$member->mem_nom}</p>
            <h4>Prénom</h4>
            <p>{$member->mem_prenom}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Modifier les informations</h3>
            <form role="form" method="post">  
                <input name="apply" value="1" type="hidden" required="required">
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" value="{$member->mem_mail}" placeholder="mail*" type="email" class="form-control" required="required">
                </div>
                 <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" value="" placeholder="password" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success" name="addGrp">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
{include file='../common/footer.tpl'}