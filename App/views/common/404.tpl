{include file='./header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Erreur 404 !
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
            {if $invalid_page === 'true'}
                <p>Cette page n'existe pas !</p>
            {else}
                {if $error_action === 'true'}
                    <p>{$action} n'est pas une action valide sur la page {$source} !</p>
                {else}
                    <p>{$source} n'est pas une page valide !</p>
                {/if}
            {/if}
    	</div>
    </div>
{include file='./footer.tpl'}