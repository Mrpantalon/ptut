<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Projet tut</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/moment-with-locales.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
		<script src="/ptut/Public/js/jquery.min.js"></script>
    	<script src="/ptut/Public/js/bootstrap.min.js"></script>
    	<script src="/ptut/Public/js/sb-admin-2.min.js"></script>
    	<script src="/ptut/Public/js/metisMenu.min.js"></script>
    	<script src="/ptut/Public/js/moment-with-locales.min.js"></script>
    	<script src="/ptut/Public/js/bootstrap-datetimepicker.min.js"></script>

    	<style>
    		body, html {
			  height: 100%;
			}
			.fill { 
				min-height: 100%;
				height: 100%;
			}
			@-moz-document url-prefix() {
			  select.form-control {
			    padding-right: 25px;
			    background-image: url("data:image/svg+xml,\
			      <svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='14px'\
			           height='14px' viewBox='0 0 1200 1000' fill='rgb(51,51,51)'>\
			        <path d='M1100 411l-198 -199l-353 353l-353 -353l-197 199l551 551z'/>\
			      </svg>");
			    background-repeat: no-repeat;
			    background-position: calc(100% - 7px) 50%;
			    -moz-appearance: none;
			    appearance: none;
			  }
			}
    	</style>
    	<!--<script src="Public/js/moment.min.js"></script>-->
    	<!--<script src="Public/js/bootstrap-datetimepicker.js"></script>-->

	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<div id="wrapper" class="fill">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="/ptut/index">Gestion Projet tutoré</a>
	            </div>

	            <ul class="nav navbar-top-links navbar-right">
		            <li class="dropdown">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #eee" onmouseover="this.style.backgroundColor='#111'" onmouseout="this.style.backgroundColor='#222'">
	                    	<span class="glyphicon glyphicon-user" aria-hidden="true"></span> {$username} <b class="caret"></b>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <a href="/ptut/options">Options du profil</a>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <a href="/ptut/connection/disconnect">Déconnexion</a>
	                        </li>
	                    </ul>
	                </li>
	            </ul>

	            <div class="navbar-default sidebar" role="navigation">
	            	<div class="sidebar-nav navbar-collapse collapse">
                		<ul class="nav" id="side-menu">
                			<li><a href="/ptut/index">Acceuil</a></li> 		
                			<li><a href="/ptut/ptuts">Projets tutoré{if $admin === true}s{/if}</a></li> 
                			{if $admin === true}								
								<li><a href="/ptut/groups">Groupes</a></li> 
								<li><a href="/ptut/members">Membres</a></li> 
							{/if}
							<li><a href="/ptut/meetings">Réunions</a></li>
                		</ul>
                	</div>
	            </div>
			</nav>
			<div id="page-wrapper" class="fill" style="margin-top: 30px">
				<div class="fill">
