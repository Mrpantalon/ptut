{include file='../common/header.tpl'}
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mes réunions
            </h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
    		{if $mettings|@count>=1}
		        <div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center">Date</th>
								<th class="text-center">Salle</th>
								<th class="text-center">Terminé</th>
								<th class="text-center">Demandeur</th>
								<th class="text-center">Priorité</th>
								<th class="text-center">PTUT</th>
							</tr>
						</thead>
						<tbody>
							{if isset($mettings)}
								{foreach from=$mettings item=it}
								<tr {if $it->reu_terminer == 1} class='danger' {else if $it->reu_terminer == 2}  class='warning' {else} class='success' {/if}>
									<td class="text-center">{$it->reu_dateheure}</td>
									<td class="text-center">{$it->reu_salle}</td>
									<td class="text-center">
										{if $it->reu_terminer == 0}
											non
										{else}
											oui
										{/if}
									</td>
									<td class="text-center">{$it->mem_id_demand}</td>
									<td class="text-center">{$it->mem_id_priorit}</td>
									<td class="text-center"><a href="#" role="button">{$ptuts[$it->reu_num]->ptu_nom}</a></td>
								</tr>
								{/foreach}
							{else}
								<p>Vous n'avez aucune réunion !</p>
							{/if}
						</tbody>
					</table>
				</div>
			{else}
			<p>Vous n'avez aucune réunions !
			{/if}
		</div>
	</div>
	{if $member->mem_chef_projet == 1 || $member->mem_statut === 'enseignant' || $member->mem_statut === 'extérieur'}
	<div class="row">
		<div class="col-lg-12">
			<form role="form">
				<div class="form-group">
					<label>Salle</label>
					<input name="room" placeholder="Salle*" type="text"  placeholder="" class="form-control" required="true">
				</div>
				{if $member->mem_statut === 'enseignant' || $member->mem_statut === 'extérieur'}
					<div class="form-group">
						<label>PTUT</label>
						<select name="applicant" id="ptut" required="true" class="form-control">
							{foreach from=$ptuts_unique item=it}
		                		<option value='{$it->ptu_num}'>{$it->ptu_nom}</option>
		                	{/foreach}
		            	</select>
					</div>
				{else}
					<input name="applicant" type="hidden" value="{$member->mem_id}" class="form-control" required="true">
				{/if}
				{if $member->mem_statut === 'enseignant' || $member->mem_statut === 'extérieur'}
					<div class="form-group">
						<label>Demandeur</label>
						<select name="applicant" id="owner" required="true" class="form-control">
							<optgroup class="active" id="toto">

							</optgroup>
		            	</select>
					</div>
				{else}
					<input name="applicant" type="hidden" value="{$member->mem_id}" class="form-control" required="true">
				{/if}
				<div class="form-group">
					<label>Priorité</label>
					<select name="priority" id="owner" required="true" class="form-control">
	                    
	                </select>
				</div>
				<div class="form-group">
					<label>Date</label>
					<div class='input-group date' id='datetimepicker'>
	            		<input name="date" type="text" required="true" class="form-control" />
	            		<span class="input-group-addon">
	                	<span class="glyphicon glyphicon-time"></span>
	            		</span>
	        		</div>
	    		</div>
				<div class="form-group">
					<label>PTUT</label>
				</div>
				<div class="form-group">
					<div class="btn-group" role="group" aria-label="...">
							<button class="btn btn-success" type='submit' formmethod='post'>Ajouter</button>
					</div>
				</div>
				<script type="text/javascript">
					$(function () {
	    				$('#datetimepicker').datetimepicker({
	    					locale: 'fr',
	    					format: "YYYY-MM-DD HH-mm",
	    					sideBySide: true,
	    				});
					});
				</script>
			</form>
		</div>
	</div>
	{/if}
{include file='../common/footer.tpl'}