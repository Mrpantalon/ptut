<?php
	namespace Modeles;

	class Group {
		private $pdo;

		public function __construct() {
			$this->pdo = \Utils\BDD::getPDO();
		}
		public function add($ptu_num, $group_name) {
			try {
				$request = $this->pdo->prepare("INSERT INTO t_groupe VALUES(default, :ptu_num, :group_name)");
				$request->execute(array('ptu_num' => $ptu_num, 'group_name' => $group_name));
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		public function removeMemberFrom($group_id, $member_id) {
			try {
				$request = $this->pdo->prepare('DELETE FROM tj_dans_groupe WHERE gro_num = :gro_num AND mem_id = :mem_id');
				$request->execute(array('gro_num' => $group_id, 'mem_id' => $member_id));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function addMemberTo($group_id, $member_id) {
			try {
				$request = $this->pdo->prepare("INSERT INTO tj_dans_groupe VALUES(:gro_num, :mem_id)");
				$request->execute(array('gro_num' => $group_id, 'mem_id' => $member_id));	
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function remove($id) {
            try {
				$request = $this->pdo->prepare('DELETE FROM tj_dans_groupe WHERE gro_num = :gro_num');
				$request->execute(array('gro_num' => $id));
				$this->removeMemberTo($id);
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function update($group_id, $group_name, $ptu_num) {
			 try {
				$request = $this->pdo->prepare('UPDATE t_groupe SET ptu_num= :ptu_num, gro_nom= :gro_nom WHERE gro_num = :group_id');
				$request->execute(array('group_id' => $group_id, 'gro_nom' => $group_name, 'ptu_num' => $ptu_num));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function getAll() {
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_groupe');
				$request->execute();
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function get($id) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_groupe WHERE gro_num = :id');
				$request->execute(array(
					'id' => $id
					)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
	}