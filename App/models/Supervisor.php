<?php
	namespace Modeles;

	class Supervisor {
		private $pdo;

		public function __construct() {
			$this->pdo = \Utils\BDD::getPDO();
		}

		public function add($ptut, $member, $role)
		{
			try{
				$request=$this->pdo->prepare("INSERT INTO tj_encadrent VALUES(:ptut, :member, :role)");
				$request->execute(array( 
										'ptut' => $ptut,
										'member' => $member,
										'role' => $role));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}	

		public function remove($ptut, $member, $role)
		{
			try{
				$request=$this->pdo->prepare("DELETE FROM tj_encadrent WHERE ptu_num=:ptutid and mem_id=:memberid and enc_role=:role");			
				$request->execute(array(
										'ptutid' => $ptut,
										'memberid' => $member,
										'role' => $role));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function edit($ptut, $member, $role)
		{

		}	

		public function get($id)
		{
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM tj_encadrent WHERE ptu_num = :id');
				$request->execute(array(
										'id' => $id));
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}

		public function getAlls()
		{
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_ptut 
														JOIN tj_encadrent ON t_ptut.ptu_num=tj_encadrent.ptu_num
														JOIN t_membre ON tj_encadrent.mem_id=t_membre.mem_id');
				$request->execute();
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}

		public function getFromUser($username)
		{
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_ptut 
														JOIN tj_encadrent ON t_ptut.ptu_num=tj_encadrent.ptu_num
														JOIN t_membre ON tj_encadrent.mem_id=t_membre.mem_id
														WHERE tj_encadrent.mem_id = :username');
				$request->execute(array(
					'username' => $username
				));
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}

		public function getMembers($statut1, $statut2)
		{
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT mem_id, mem_nom FROM t_membre WHERE mem_statut=:statut1 or mem_statut=:statut2');
				$request->execute(array(
										'statut1' => $statut1,
										'statut2' => $statut2));
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
	}
