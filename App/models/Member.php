<?php
	namespace Modeles;

	class Member {
		private $pdo;

		public function __construct() {
			$this->pdo = \Utils\BDD::getPDO();
		}
		public function exist($login) {
			var_dump($this->get($login));
			return $this->get($login) != null;
		}
		public function add($name, $firstname, $mail, $status, $login, $password, $project_owner, $student) {
			try {
				$request = $this->pdo->prepare('INSERT INTO t_membre VALUES(default, :name, :firstname, :mail, :status, :login, :password, :project_owner, :student)');
				$request->execute(array(
					'name' => $name,
					'firstname' => $firstname,
					'mail' => $mail,
					'status' => $status,
					'login' => $login,
					'password' => $password,
					'project_owner' => $project_owner,
					'student' => $student,
					));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function remove($member_id) {
			try {
                $request=$this->pdo->prepare("UPDATE t_reunion SET mem_id_demand='1' WHERE mem_id_demand= :mem_id");            
                $request->execute(array('mem_id' => $member_id));
                $request=$this->pdo->prepare("UPDATE t_reunion SET mem_id_priorit='1' WHERE mem_id_priorit= :mem_id");            
                $request->execute(array('mem_id' => $member_id));
				$request=$this->pdo->prepare("DELETE FROM tj_encadrent WHERE mem_id = :mem_id");            
                $request->execute(array('mem_id' => $member_id));                
                $request=$this->pdo->prepare("DELETE FROM tj_dans_groupe WHERE mem_id = :mem_id");            
                $request->execute(array('mem_id' => $member_id));
				$request = $this->pdo->prepare('DELETE FROM t_membre WHERE mem_id = :mem_id');
				$request->execute(array('mem_id' => $member_id));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function update($member_id, $mail, $password) {
			try {
				if(empty($password)) {
					$request = $this->pdo->prepare('UPDATE t_membre SET mem_mail = :mail WHERE mem_id = :mem_id');
					$request->execute(array('mem_id' => $member_id, 'mail' => $mail));
				} else {
					$request = $this->pdo->prepare('UPDATE t_membre SET mem_mail = :mail, mem_password = :password WHERE mem_id = :mem_id');
					$request->execute(array('mem_id' => $member_id, 'mail' => $mail, 'password' => sha1($password)));
				}
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function updateFull($member_id, $name, $firstname, $statut, $project_owner, $login, $mail, $password, $student) {
			try {
				$request = $this->pdo->prepare('
					UPDATE t_membre SET mem_nom = :name, mem_prenom = :firstname, mem_mail= :mail, mem_statut = :statut, mem_chef_projet = :projet_owner, mem_login = :login, mem_password = :password, mem_etudiant = :student WHERE mem_id = :mem_id');
				$request->execute(array('mem_id' => $member_id, 'name' => $name, 'firstname' => $firstname, 'statut' => $statut, 'projet_owner' => $project_owner, 'login' => $login,  'mail' => $mail, 'student' => $student, 'password' => sha1($password)));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function getMembersFromGroup($group_id) {
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT
				 t_membre.mem_id, t_membre.mem_nom,
				 t_membre.mem_prenom, t_membre.mem_mail,
				 t_membre.mem_statut, t_membre.mem_login,
				 t_membre.mem_password, t_membre.mem_chef_projet,
				 t_membre.mem_etudiant FROM t_membre JOIN tj_dans_groupe ON t_membre.mem_id = tj_dans_groupe.mem_id WHERE tj_dans_groupe.gro_num = :gro_num');
				$request->execute(array('gro_num' => $group_id));
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getAll() {
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_membre');
				$request->execute();
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function get($username) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_membre WHERE mem_login = :username');
				$request->execute(array(
					'username' => $username
					)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getFromID($id) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_membre WHERE mem_id = :id');
				$request->execute(array(
					'id' => $id
					)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
	}