<?php
	namespace Modeles;

	class PTUT {
		private $pdo;

		public function __construct() {
			$this->pdo = \Utils\BDD::getPDO();
		}
		public function edit($id, $newname, $newdescription, $newdirectory)
		{
			try
			{
				$request=$this->pdo->prepare("UPDATE t_ptut SET ptu_nom=:newname, ptu_description=:newdescription, ptu_dossier_racine=:newdirectory
															WHERE ptu_num=:id");
				$request->execute(array(
										'id' => $id,
										'newname' => $newname,
										'newdescription' => $newdescription,
										'newdirectory' => $newdirectory));
			}
			catch(PDOException $e) {
				die($e->getMessage());
			}			
		}
		public function add() {
			try
			{
				$request=$this->pdo->prepare("INSERT INTO t_ptut VALUES(default, :name, :description, default, :directory)");
                $request->execute(array(
                                        'name' => $_POST['name'],
                                        'description' => stripslashes($_POST['description']), 
                                        'directory' => $_POST['directory']));
			}
			catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function remove($num) {			
			try
			{					
				$request= $this->pdo->prepare("SELECT tj_reu_ptut.reu_num FROM tj_reu_ptut
										JOIN t_reunion ON tj_reu_ptut.reu_num=t_reunion.reu_num
										JOIN t_ordre_jour ON t_reunion.reu_num=t_ordre_jour.reu_num
										JOIN t_compte_rendu ON t_ordre_jour.ord_num=t_compte_rendu.ord_num
										WHERE tj_reu_ptut.ptu_num= :num");		
				$request->execute(array( 
										'num' => $num));			
				$reu = $request->fetchAll(\PDO::FETCH_COLUMN, 0);

				$request= $this->pdo->prepare("SELECT tj_note.rev_num FROM tj_note
											JOIN t_ptut ON t_ptut.ptu_num=tj_note.ptu_num
											WHERE t_ptut.ptu_num=:num");
				$request->execute(array( 
										'num' => $num));
				$rev = $request->fetchAll(\PDO::FETCH_COLUMN, 0);

				$request= $this->pdo->prepare("DELETE FROM tj_note WHERE ptu_num= :num");			
				$request->execute(array(
										 'num' => $num));

				foreach ($rev as $revu)
				{
					$request= $this->pdo->prepare("DELETE FROM t_revue WHERE rev_num= :revu");			
					$request->execute(array(
											'revu' => $revu));
				}

				$request= $this->pdo->prepare("DELETE FROM t_fichier WHERE ptu_num= :num");			
				$request->execute(array( 
										'num' => $num));

				$request= $this->pdo->prepare("DELETE FROM tj_reu_ptut WHERE ptu_num= :num");			
				$request->execute(array( 
										'num' => $num));

				foreach ($reu as $all)
				{
					$request= $this->pdo->prepare("DELETE FROM t_compte_rendu WHERE ord_num= :all");			
					$request->execute(array( 
											'all' => $all));	
					$request= $this->pdo->prepare("DELETE FROM t_ordre_jour WHERE reu_num= :all");			
					$request->execute(array( 
											'all' => $all));									
					$request= $this->pdo->prepare("DELETE FROM t_reunion WHERE reu_num= :all");			
					$request->execute(array( 
											'all' => $all));
				}		

				$request= $this->pdo->prepare("UPDATE t_groupe SET ptu_num=1 WHERE ptu_num= :num");			
				$request->execute(array( 
										'num' => $num));

				$request= $this->pdo->prepare("UPDATE tj_encadrent SET ptu_num=1 WHERE ptu_num= :num");			
				$request->execute(array( 
										'num' => $num));

				$request= $this->pdo->prepare("DELETE FROM t_ptut WHERE ptu_num= :num");			
				$request->execute(array( 
										'num' => $num));				
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll() {
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_ptut');
				$request->execute();
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function get($id) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_ptut WHERE ptu_num = :id');
				$request->execute(array(
					'id' => $id
				));
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getFromMeeting($meeting_id) {
			$result = '';
			try {
				$request = $this->pdo->prepare(
					'SELECT 
					t_ptut.ptu_num,t_ptut.ptu_nom, t_ptut.ptu_description,
					t_ptut.ptu_date_creation, t_ptut.ptu_dossier_racine
					FROM t_ptut JOIN tj_reu_ptut ON t_ptut.ptu_num    = tj_reu_ptut.ptu_num
								JOIN t_reunion   ON t_reunion.reu_num = tj_reu_ptut.reu_num 
                    WHERE tj_reu_ptut.reu_num = :meeting_id');
				$request->execute(array('meeting_id' => $meeting_id));

				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getFromUser($username) {
			$username=$_SESSION['username'];
			$result = '';
			try {
				$request = $this->pdo->prepare('
					SELECT 
					t_ptut.ptu_num,t_ptut.ptu_nom, t_ptut.ptu_description,
					t_ptut.ptu_date_creation,t_ptut.ptu_dossier_racine 
					FROM t_ptut JOIN t_groupe ON t_ptut.ptu_num=t_groupe.ptu_num 
								JOIN tj_dans_groupe ON t_groupe.gro_num = tj_dans_groupe.gro_num
								JOIN t_membre ON        t_membre.mem_id = tj_dans_groupe.mem_id
					WHERE mem_login = :username');
				$request->execute(array(
					'username' => $username)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getMemberFrom($id) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_ptut JOIN t_groupe ON       t_ptut.ptu_num   = t_groupe.ptu_num 
																	 JOIN tj_dans_groupe ON t_groupe.gro_num = tj_dans_groupe.gro_num
																	 JOIN t_membre ON       t_membre.mem_id  = tj_dans_groupe.mem_id
												WHERE t_ptut.ptu_num = :id');
				$request->execute(array(
					'id' => $id)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
	}
