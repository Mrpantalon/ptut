<?php
	namespace Modeles;

	class Meeting {
		private $pdo;

		public function __construct() {
			$this->pdo = \Utils\BDD::getPDO();
		}
		public function add() {

		}
		public function remove($reu_num) {
			try {
				$request = $this->pdo->prepare("SELECT * FROM t_ordre_jour WHERE reu_num = :reu_num");    
		        $request->execute(array('reu_num' => $reu_num));
		        $ord_num = $request->fetchAll(\PDO::FETCH_CLASS);

				$request = $this->pdo->prepare("DELETE FROM t_compte_rendu WHERE ord_num = :ord_num");    
            	$request->execute(array('ord_num' => $ord_num[0]->ord_num));

        		$request = $this->pdo->prepare("DELETE FROM t_ordre_jour WHERE reu_num = :reu_num");    
        		$request->execute(array('reu_num' => $reu_num));
       			$request = $this->pdo->prepare("DELETE FROM tj_reu_ptut WHERE reu_num = :reu_num");    
        		$request->execute(array('reu_num' => $reu_num));
				$request = $this->pdo->prepare('DELETE FROM t_reunion WHERE reu_num = :reu_num');
				$request->execute(array('reu_num' => $reu_num));
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function edit() {
			
		}
		public function getAll() {
			$result = array();
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_reunion');
				$request->execute();
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function getFromUser($username) {
			$result = '';
			try {
				$request = $this->pdo->prepare(
					'SELECT 
					t_reunion.reu_num, t_reunion.reu_dateheure,
					t_reunion.reu_salle, t_reunion.reu_terminer,
					t_reunion.mem_id_demand, t_reunion.mem_id_priorit
					FROM t_reunion JOIN tj_reu_ptut ON t_reunion.reu_num = tj_reu_ptut.reu_num 
								   JOIN t_ptut ON         t_ptut.ptu_num    = tj_reu_ptut.ptu_num
								   JOIN t_groupe ON       t_groupe.ptu_num  = t_ptut.ptu_num 
                                   JOIN tj_dans_groupe ON t_groupe.gro_num  = tj_dans_groupe.gro_num
                                   JOIN t_membre ON       t_membre.mem_id   = tj_dans_groupe.mem_id
                    WHERE t_membre.mem_login = :username');
				$request->execute(array(
					'username' => $username)
				);

				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
		public function get($id) {
			$result = '';
			try {
				$request = $this->pdo->prepare('SELECT * FROM t_reunion WHERE reu_num = :id');
				$request->execute(array(
					'id' => $id)
				);
				$result = $request->fetchAll(\PDO::FETCH_CLASS);
			} catch(PDOException $e) {
				die($e->getMessage());
			}

			return $result;
		}
	}