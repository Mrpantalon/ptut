<?php
    namespace Controllers;

    require_once(dirname( __FILE__ ) . "/BaseController.php");
    require_once(dirname( __FILE__ ) . '/../utils/BDD.php');
    require_once(dirname( __FILE__ ) . '/../models/Supervisor.php');

    class SupervisorController extends BaseController {
        private $SupervisorDB;

        public function __construct() {
            parent::__construct();

            $this->SupervisorDB = new \Modeles\Supervisor();
        }               

        public function routeAction($action, $smarty) {           
            if($action === 'add')
                $this->add($smarty);
            else if($action === 'remove')
                $this->remove();
            else if($action === 'edit')
                $this->edit($smarty);                 
            else
                throw new ActionRouteException($action);
        }  

        public function index($smarty)
        {            
            
        }      

        public function add($smarty)
        {             
            $id=htmlentities($_POST['ptut_num']);          
            if(isset($_POST['apply']) && $_POST['apply'] == 1) {                
                $member=htmlentities($_POST['memberid']);
                $role=htmlentities($_POST['role']);                
                $this->SupervisorDB->add($id, $member, $role);
                header('Location: /ptut/ptuts/index');
                exit();
            } else {               
                $extern='exterieur';
                $techears='enseignant';                
                $smarty->assign('members', $this->SupervisorDB->getMembers($extern, $techears));                
                $smarty->display('supervisors/index.tpl');
            }            
        }

        public function edit($smarty)
        {

        }

        public function remove()
        {
            $member=htmlentities($_POST['memberid']);
            $ptut=htmlentities($_POST['ptut_num']);
            $role=htmlentities($_POST['role']);
            $this->SupervisorDB->remove($ptut, $member, $role);
        }        
    }
?>