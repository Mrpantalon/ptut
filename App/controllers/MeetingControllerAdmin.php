<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . '/BaseController.php');
	require_once(dirname( __FILE__ ) . '/../models/Meeting.php');
	require_once(dirname( __FILE__ ) . '/../models/Member.php');
	require_once(dirname( __FILE__ ) . '/../models/PTUT.php');
	require_once(dirname( __FILE__ ) . '/../utils/BDD.php');

	class MeetingControllerAdmin extends BaseController {
		private $meetingDB;
		private $memberDB;
		private $ptutDB;

		public function __construct() {
			parent::__construct();

			$this->meetingDB = new \Modeles\Meeting();
			$this->memberDB = new \Modeles\Member();
			$this->ptutDB = new \Modeles\PTUT();
		}
 
		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else if($action === 'add') {
				$this->add();
				$this->index($smarty);
			} else if($action === 'remove') {
				$this->remove();
				$this->index($smarty);
			} else
				throw new ActionRouteException($action);
		}
		public function index($smarty) {
			$username = $_SESSION['username'];

			$meetings = $this->meetingDB->getAll();

			$ptuts = array();
			foreach ($meetings as $key => $value) {
				$ptut = $this->ptutDB->getFromMeeting($value->reu_num);
				$ptuts[$value->reu_num] = $ptut[0];
			}

			$smarty->assign('mettings', $meetings);
			$smarty->assign('ptuts', $ptuts);
			$smarty->assign('ptuts_unique', array_unique($ptuts, SORT_REGULAR));
			$smarty->assign('ptut_name', 'dummy');
			$smarty->display('meetings/index_admin.tpl');
		}
		public function add() {
			
		}

		public function remove() {
			$reu_id = htmlentities($_POST['reu_id']);

			$this->meetingDB->remove($reu_id);
		}
	}