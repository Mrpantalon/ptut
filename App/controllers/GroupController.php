<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . "/BaseController.php");
	require_once(dirname( __FILE__ ) . '/../models/Group.php');
	require_once(dirname( __FILE__ ) . '/../models/Member.php');
	require_once(dirname( __FILE__ ) . '/../models/PTUT.php');

	function array_difference($array1,$array2){
         $result=array();
         foreach($array1 as $a1){
             if(!in_array($a1,$array2)){
                 array_push($result,$a1);
             }
         }
         return $result;
     }

	class GroupController extends BaseController {
		private $groupDB;
		private $memberDB;
		private $ptutDB;
 
		public function __construct() {
			parent::__construct();

			$this->groupDB = new \Modeles\Group();
			$this->memberDB = new \Modeles\Member();
			$this->ptutDB = new \Modeles\PTUT();
		}

		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else if($action === 'remove') {
				$this->remove();
				$this->index($smarty);
			} else if($action === 'add') {
				$this->add();
				$this->index($smarty);
			}  else if($action === 'edit') {
				$this->edit($smarty);
			} else
				throw new ActionRouteException($action);
		}

		public function index($smarty) {
			$groups = $this->groupDB->getAll();

			$ptuts_grp = array();

			foreach($groups as $key => $value) {
				$ptut = $this->ptutDB->get($value->ptu_num);
				$ptuts_grp[$value->ptu_num] = $ptut[0];
			}

			$smarty->assign('groups', $groups);
			$smarty->assign('ptuts_grp', $ptuts_grp);
			$smarty->assign('ptuts', $this->ptutDB->getAll());
			$smarty->display('groups/index.tpl');
		}

		public function add() {
			$ptu_num = htmlentities($_POST['ptu_num']);
			$group_name = htmlentities($_POST['group_name']);

			$this->groupDB->add($ptu_num, $group_name);
		}

		public function edit($smarty) {
            if(isset($_POST['apply']) && $_POST['apply'] == 1) {
                $group_id  = htmlentities($_POST['group_num']);
                $group_name = htmlentities($_POST['group_name']);
                $ptu_num = htmlentities($_POST['ptu_num']);

                $this->groupDB->update($group_id, $group_name, $ptu_num);

                header('Location: /ptut/groups/index');
				exit();
            } else {
                $group_name = htmlentities($_POST['group_name']);
                $ptu_num = htmlentities($_POST['ptu_num']);
                $gro_num = htmlentities($_POST['gro_num']);

                if(isset($_POST['add_member']) && $_POST['add_member'] == 1) {
                    $member_id = htmlentities($_POST['member_id']);

                    $this->groupDB->addMemberTo($gro_num, $member_id);
                } else if (isset($_POST['remove_member']) && $_POST['remove_member'] == 1) {
                    $member_id = htmlentities($_POST['member_id']);

                    var_dump($member_id);

                    $this->groupDB->removeMemberFrom($gro_num, $member_id);
                }

                $smarty->assign('gro_num', $gro_num);
                $smarty->assign('group_name', $group_name);
                $smarty->assign('ptu_num', $ptu_num);
                $smarty->assign('ptuts', $this->ptutDB->getAll());
                $smarty->assign('members_group', $this->memberDB->getMembersFromGroup($gro_num));
                $smarty->assign('members', array_difference($this->memberDB->getAll(), $this->memberDB->getMembersFromGroup($gro_num)));
                 $smarty->display('groups/edit.tpl');
             }
        }

		public function remove() {
			$id = htmlentities($_POST['id']);

			$this->groupDB->remove($id);
		}
	}
?>