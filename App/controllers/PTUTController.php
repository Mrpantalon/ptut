<?php
	namespace Controllers;

    require_once(dirname( __FILE__ ) . "/BaseController.php");
    require_once(dirname( __FILE__ ) . '/../utils/BDD.php');
    require_once(dirname( __FILE__ ) . '/../models/PTUT.php');

    class PTUTController extends BaseController {
    	private $ptutDB;

    	public function __construct() {
			parent::__construct();

			$this->ptutDB = new \Modeles\PTUT();
		}

        public function routeAction($action, $smarty) {
            if($action === '' || $action === 'index')
                $this->index($smarty);
            else
                throw new ActionRouteException($action);
        }
        public function index($smarty) {
            $username=$_SESSION['username'];
            $ptut = $this->ptutDB->getFromUser($username);
            $smarty->assign('ptut', $ptut);

            $members = array();
            foreach ($ptut as $key => $value) {
                        $ptut_num=$value->ptu_num;
                        $members[$ptut_num] = $this->ptutDB->getMemberFrom($ptut_num);
            }

            $smarty->assign('members', $members);                  
            $smarty->display('ptuts/index.tpl');
        }
    }
?>