<?php
	namespace Controllers;
	
	class ActionRouteException extends \Exception {
		public function __construct($message, $code = 0, Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

		public function __toString() {
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
		}
	}

	abstract  class BaseController {
		function __construct() {

		}

		abstract public function routeAction($action, $smarty);
		abstract public function index($smarty);
	}