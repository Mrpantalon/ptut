<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . "/BaseController.php");

	class Controller404 extends BaseController {
		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else
				throw new ActionRouteException($action);
		}
		public function index($smarty) {
			$smarty->display('common/404.tpl');
		}
	}
