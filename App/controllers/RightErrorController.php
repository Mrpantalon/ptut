<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . "/BaseController.php");

	class RightErrorController extends BaseController {
		public function routeAction($action, $smarty) {
			$this->index($smarty);
		}
		public function index($smarty) {
			$smarty->display('common/error_right.tpl');
		}
	}
?>