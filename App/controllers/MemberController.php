<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . "/BaseController.php");

	class MemberController extends BaseController {
		private $memberDB;

		public function __construct() {
			parent::__construct();

			$this->memberDB = new \Modeles\Member();
		}

		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else if($action === 'add') {
				$this->add($smarty);
				$this->index($smarty);
			} else if($action === 'remove') {
				$this->remove();
				$this->index($smarty);
			} else if($action === 'edit') {
				$this->edit($smarty);
			} else
				throw new ActionRouteException($action);
		}
		public function index($smarty) {
			$members = $this->memberDB->getAll();

			$smarty->assign('members', $members);
			$smarty->display('members/index.tpl');
		}
		public function remove() {
			$id = $_POST['member_id'];

			$this->memberDB->remove($id);
		}
		public function edit($smarty) {
			$mem_id = htmlentities($_POST['member_id']);

			if(isset($_POST['apply']) && $_POST['apply'] == 1) {
				$name = htmlentities($_POST['member_name']);
				$firstname = htmlentities($_POST['member_firstname']);
				$mail = htmlentities($_POST['member_mail']);
				$status = htmlentities($_POST['member_status']);
				$login = htmlentities($_POST['member_login']);
				if(isset($_POST['member_projet_owner']))
					$project_owner = htmlentities($_POST['member_projet_owner']);
				else
					$project_owner = null;
				$password = sha1($_POST['member_password']);

				if($status === "etudiant")
					$student = 1;
				else
					$student = null;

				$this->memberDB->updateFull($mem_id, $name, $firstname, $status, $project_owner, $login, $mail, $password, $student);

				header('Location: /ptut/members/index');
				exit();
			} else {
				$member = $this->memberDB->getFromID($mem_id);

				$smarty->assign('member_name', $member[0]->mem_nom);
				$smarty->assign('member_firstname', $member[0]->mem_prenom);
				$smarty->assign('member_mail', $member[0]->mem_mail);
				$smarty->assign('member_status', $member[0]->mem_statut);
				$smarty->assign('member_projet_owner', $member[0]->mem_chef_projet);
				$smarty->assign('member_login', $member[0]->mem_login);
				$smarty->assign('member_id', $mem_id);
				$smarty->display('members/edit.tpl');
			}
		}
		public function add($smarty) {
			$name = htmlentities($_POST['member_name']);
			$firstname = htmlentities($_POST['member_firstname']);
			$mail = htmlentities($_POST['member_mail']);
			$status = htmlentities($_POST['member_status']);
			$login = htmlentities($_POST['member_login']);
			$password = sha1($_POST['member_password']);

			if(isset($_POST['member_projet_owner']))
				$project_owner = htmlentities($_POST['member_projet_owner']);
			else
				$project_owner = null;

			if($status === "etudiant")
				$student = 1;
			else
				$student = null;

			if($this->memberDB->exist($login)) {
				$smarty->assign('err_login', 1);
				return;
			}

			$this->memberDB->add($name, $firstname, $mail, $status, $login, $password, $project_owner, $student);
		}
	}
?>