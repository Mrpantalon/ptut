<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . '/BaseController.php');
	require_once(dirname( __FILE__ ) . '/../utils/BDD.php');
	require_once(dirname( __FILE__ ) . '/../utils/SessionManager.php');
	require_once(dirname( __FILE__ ) . '/../models/Member.php');

	class ConnectionException extends \Exception {
		public function __construct($message, $code = 0, Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

		public function __toString() {
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
		}
	}

	class ConnectionController extends BaseController {
		private $memberDB;

		public function __construct() {
			parent::__construct();

			$this->memberDB = new \Modeles\Member();
		}
		
		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else if($action === 'connect')
				$this->connect($smarty);
			else if($action === 'disconnect')
				$this->disconnect($smarty);
			else
				throw new ActionRouteException($action);
		}

		public function index($smarty) {
			$smarty->display('connection/index.tpl');
		}

		public function connect($smarty) {	
			try {
				if(!isset($_POST['username']) || empty($_POST['username'])
				|| !isset($_POST['password']) || empty($_POST['password']))
					throw new ConnectionException('Identifiant de connexion ou mot de passe manquant.');

				$password = sha1($_POST['password']);
				$username = $_POST['username'];
				//var_dump($_POST['password']);

				$member_password = $this->memberDB->get($username);
				if($member_password == NULL)
					throw new ConnectionException('Identifiant de connection de connexion incorrect.');
				if($member_password[0]->mem_password !== $password)
					throw new ConnectionException('Mot de passe incorrect.');

				\Utils\SessionManager::restoreSession();
				$_SESSION['username'] = strtolower($username);

				$smarty->display('connection/connect_success.tpl');
			} catch(ConnectionException $e) {
				$smarty->assign('error', $e->getMessage());
				$smarty->display('connection/connect_failed.tpl');
			}
		}

		public function disconnect($smarty) {
			$_SESSION = array();
			\Utils\SessionManager::destroySession();
			setcookie('username', '');
			setcookie('password', '');

			$smarty->display('connection/disconnect.tpl');
		}
	}