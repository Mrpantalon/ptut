<?php
	namespace Controllers;

	require_once(dirname( __FILE__ ) . "/BaseController.php");
	require_once(dirname( __FILE__ ) . '/../models/Member.php');

	class OptionController extends BaseController {
		private $memberDB;

		public function __construct() {
			parent::__construct();

			$this->memberDB = new \Modeles\Member();
		}

		public function routeAction($action, $smarty) {
			if($action === '' || $action === 'index')
				$this->index($smarty);
			else
				throw new ActionRouteException($action);
		}
		public function index($smarty) {
			$member = $this->memberDB->get($_SESSION['username']);

			if(isset($_POST['apply']) && $_POST['apply'] == 1) {
				$mail = htmlentities($_POST['member_mail']);
				$password = htmlentities($_POST['member_password']);

				$this->memberDB->update($member[0]->mem_id, $mail, $password);

				$smarty->assign('success', 1);
			}

			$member = $this->memberDB->get($_SESSION['username']);

			$smarty->assign('member', $member[0]);
			$smarty->display('option/index.tpl');
		}
	}
?>