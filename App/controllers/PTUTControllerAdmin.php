<?php
	namespace Controllers;

    require_once(dirname( __FILE__ ) . "/BaseController.php");
    require_once(dirname( __FILE__ ) . '/../utils/BDD.php');
    require_once(dirname( __FILE__ ) . '/../models/PTUT.php');

    class PTUTControllerAdmin extends BaseController {
    	private $ptutDB;

    	public function __construct() {
			parent::__construct();

			$this->ptutDB = new \Modeles\PTUT();
		}

        public function remove()
        {
            $num=htmlentities($_POST['ptut_num']);
            $this->ptutDB->remove($num);
        }

        public function add() {
            $dirname = htmlentities($_POST['directory']);
            
            $this->ptutDB->add();

            if(!is_dir(dirname( __FILE__ ) . '/../storage/' . $dirname ))
                mkdir(dirname( __FILE__ ) . '/../storage/' . $dirname, 0777);
        }        

        public function routeAction($action, $smarty) {
            if($action === '' || $action === 'index')
                $this->index($smarty);
            else if($action === 'add'){
                $this->add();
                $this->index($smarty);}
            else if($action === 'remove'){
                $this->remove();
                $this->index($smarty);}
            else if($action === 'edit')
                $this->edit($smarty);           
            else
                throw new ActionRouteException($action);
        }
        public function index($smarty) {            
            $ptuts = $this->ptutDB->getAll();
            $smarty->assign('ptuts', $ptuts);
            
            $smarty->display('ptuts/index_admin.tpl');
        }

        public function edit($smarty) {  
            $id=htmlentities($_POST['ptut_num']);          
            if(isset($_POST['apply']) && $_POST['apply'] == 1) {                
                $name  = htmlentities($_POST['name']);
                $description = htmlentities(stripcslashes($_POST['description']));
                $directory = htmlentities($_POST['directory']);
                $olddirectory = $this->ptutDB->get($id)[0]->ptu_dossier_racine;
                
                $this->ptutDB->edit($id, $name, $description, $directory);
                rename(dirname( __FILE__ ) .  '/../storage/' . $olddirectory, dirname( __FILE__ ) .  '/../storage/' .$directory);

                header('Location: /ptut/ptuts/index');
                exit();
            } else {               

                $smarty->assign('ptut', $this->ptutDB->get($id)[0]);
                $smarty->display('ptuts/edit.tpl');
            }
        }
    }
?>