<?php
	namespace Utils;
	
	class SessionManager {
		public static function startSession() {
			session_start();
		}
		public static function restoreSession() {
			if(!SessionManager::isSessionActive())
				SessionManager::startSession();
		}
		
		public static function destroySession() {
			if(SessionManager::isSessionActive())
				session_destroy();
		}
		
		public static function isSessionActive() {
			return session_status() === PHP_SESSION_ACTIVE ? true : false;
		}
	}