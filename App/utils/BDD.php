<?php
	namespace Utils;
	
	class BDD {
		static private $pdo;

		public static function init($bdd_url, $bdd_port, $bdd_name, $bdd_username, $bdd_password) {
			$params = 'mysql:host='.$bdd_url.';port='.$bdd_port.';dbname='.$bdd_name;
			BDD::$pdo = new \PDO($params, $bdd_username, $bdd_password);
			BDD::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}

		public static function getPDO() {
			return BDD::$pdo;
		}

		public static function disconnect() {

		}
	}