<?php
	namespace Utils;

	require_once(dirname( __FILE__ ) . '/../deps/smarty/Smarty.class.php');
	require_once(dirname( __FILE__ ) . '/BDD.php');
	require_once(dirname( __FILE__ ) . '/../controllers/IndexController.php'); 
	require_once(dirname( __FILE__ ) . '/../controllers/ConnectionController.php'); 
 	require_once(dirname( __FILE__ ) . '/../controllers/MeetingController.php'); 
 	require_once(dirname( __FILE__ ) . '/../controllers/MeetingControllerAdmin.php'); 
    require_once(dirname( __FILE__ ) . '/../controllers/404Controller.php'); 
 	require_once(dirname( __FILE__ ) . '/../controllers/PTUTController.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/PTUTControllerAdmin.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/GroupController.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/RightErrorController.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/SupervisorController.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/OptionController.php');
 	require_once(dirname( __FILE__ ) . '/../controllers/MemberController.php');

	class ControllerRouteException extends \Exception {
		public function __construct($message, $code = 0, Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

		public function __toString() {
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
		}
	}

	class Router {
		private $smarty;
		private $indexController;
		private $meetingController;
		private $_404Controller;
		private $connectionController;
		private $PTUTController;
		private $groupController;
		private $optionController;
		private $controllerMap;
		private $supervisorController;
		private $memberController;

		public function __construct() {
			\Utils\SessionManager::restoreSession();

			$this->smarty = new \Smarty();
			$this->smarty->setTemplateDir(dirname( __FILE__ ) .'/../views/');
			$this->smarty->setCompileDir(dirname( __FILE__ ) .'/../view_compiled/');
			$this->smarty->setCacheDir(dirname( __FILE__ ) .'/../cache/');

			try {
				BDD::init('localhost', '3306', 'ptut', 'u_projet', 'SJzEeqLb2HHeNYVV');//SJzEeqLb2HHeNYVV
			} catch(PDOException $e) {
				die($e.getMessage());
			}

			if(isset($_SESSION['username']) && $_SESSION['username'] === 'admin') {
				$this->meetingController = new \Controllers\MeetingControllerAdmin();
				$this->ptutController = new \Controllers\PTUTControllerAdmin();
				$this->groupController = new \Controllers\GroupController();
				$this->supervisorController = new \Controllers\SupervisorController();
				$this->memberController = new \Controllers\MemberController();
			} else {
				$this->meetingController = new \Controllers\MeetingController();
				$this->ptutController = new \Controllers\PTUTController();
				$this->groupController = new \Controllers\RightErrorController();
				$this->supervisorController = new \Controllers\RightErrorController();
				$this->memberController = new \Controllers\RightErrorController();
			}
	    	$this->indexController = new \Controllers\IndexController();
	    	$this->connectionController = new \Controllers\ConnectionController();
	    	$this->_404Controller   = new \Controllers\Controller404();
	    	$this->optionController = new \Controllers\OptionController();

	    	$this->controllerMap            = array();
	    	$this->controllerMap['index']   = $this->indexController;
	    	$this->controllerMap['connection'] = $this->connectionController;
	    	$this->controllerMap['meetings'] = $this->meetingController;
	    	$this->controllerMap['404']     = $this->_404Controller;
	    	$this->controllerMap['ptuts']    = $this->ptutController;
	    	$this->controllerMap['groups'] = $this->groupController;
	    	$this->controllerMap['supervisors'] = $this->supervisorController;
	    	$this->controllerMap['options'] = $this->optionController;
	    	$this->controllerMap['members'] = $this->memberController;
	  	}

		public function route() {
			if(isset($_GET['invalid_page']) && $_GET['invalid_page'] == true) {
				$this->smarty->assign('invalid_page', 'true');
				$current_controller = $this->controllerMap['404'];
				$current_controller->routeAction('index', $this->smarty);

				return;
			}
			$this->smarty->assign('invalid_page', 'false');

			$source = '';
			if(empty($_GET['source']))
				$source = 'index';
			else
				$source = $_GET['source'];

			$action = '';
			if(!empty($_GET['action']))
				$action = $_GET['action'];

			if(!isset($_SESSION['username']) || empty($_SESSION['username'])) {
				if($source != 'connection')
					$source = 'connection';
				if($action !== 'connect' && $action != 'index')
					$action = 'index';
			} else {
				$admin = false;
				if($_SESSION['username'] === 'admin')
					$admin = true;
				$this->smarty->assign('admin', $admin);
				$this->smarty->assign('username',  $_SESSION['username']);
			}

			$this->smarty->assign('source', $source);
			$this->smarty->assign('action', $action);

			try {
				if(array_key_exists($source, $this->controllerMap)) {
					$current_controller = $this->controllerMap[$source];
					$current_controller->routeAction($action, $this->smarty);
				} else
					throw new ControllerRouteException($source);
			} catch(ControllerRouteException $e) {
				$this->smarty->assign('error_action', 'false');
				$current_controller = $this->controllerMap['404'];
				$current_controller->routeAction('index', $this->smarty);
			} catch(ActionRouteException $e) {
				$this->smarty->assign('error_action', 'true');
				$current_controller = $this->controllerMap['404'];
				$current_controller->routeAction('index', $this->smarty);
			} catch(Exception $e) {
				die($e->getMessage());
			}
		}
	}