<?php
/* Smarty version 3.1.30, created on 2017-03-24 01:06:53
  from "/usr/local/var/www/htdocs/ptut/App/views/ptuts/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4712d78f1a7_69215185',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '09f077144a0d1fb3eae9c56a34ec11271f0ed832' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/ptuts/edit.tpl',
      1 => 1490317582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4712d78f1a7_69215185 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un Projet Tutoré
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		<form role="form" method='post'>
                <input name="apply" type="hidden" value="1">
                <input name="ptut_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['ptut']->value->ptu_num;?>
">
                <div class="form-group">
                    <label>Nom</label>
                    <input name="name" value="<?php echo $_smarty_tpl->tpl_vars['ptut']->value->ptu_nom;?>
" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Description</label>  
                    <input name="description" value="<?php echo $_smarty_tpl->tpl_vars['ptut']->value->ptu_description;?>
" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Nom du dossier</label>  
                    <input name="directory" value="<?php echo $_smarty_tpl->tpl_vars['ptut']->value->ptu_dossier_racine;?>
" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                        <a href="/ptut/ptuts/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>			
    	</div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
