<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:44:16
  from "/usr/local/var/www/htdocs/ptut/App/views/members/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4f88077f6a7_72963354',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e9b2b2198e7eea0102916d2e58cfbdb83f453fb' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/members/edit.tpl',
      1 => 1490352244,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4f88077f6a7_72963354 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un membre
            </h1>
        </div>
    </div>	
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method="post" action="/ptut/members/edit">  
                <input type="hidden" name="member_id" value="<?php echo $_smarty_tpl->tpl_vars['member_id']->value;?>
">
                <input type="hidden" name="apply" value="1">
                <div class="form-group">
                    <label>Nom</label>
                    <input name="member_name" value="<?php echo $_smarty_tpl->tpl_vars['member_name']->value;?>
" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Prénom</label>
                    <input name="member_firstname" value="<?php echo $_smarty_tpl->tpl_vars['member_firstname']->value;?>
" placeholder="Prénom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" value="<?php echo $_smarty_tpl->tpl_vars['member_mail']->value;?>
" placeholder="Email*" type="mail" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Statut</label>
                    <select class="form-control" name="member_status">
                        <option <?php if ($_smarty_tpl->tpl_vars['member_status']->value == "enseignant") {?> selected="selected"  <?php }?>>enseignant</option>
                        <option <?php if ($_smarty_tpl->tpl_vars['member_status']->value == "étudiant") {?> selected="selected"  <?php }?>>étudiant</option>
                        <option <?php if ($_smarty_tpl->tpl_vars['member_status']->value == "extérieur") {?> selected="selected"  <?php }?>>extérieur</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Identifiant de connexion</label>
                    <input name="member_login" value="<?php echo $_smarty_tpl->tpl_vars['member_login']->value;?>
" placeholder="Identifiant*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>
                        Chef de projet <input name="member_projet_owner" <?php if ($_smarty_tpl->tpl_vars['member_projet_owner']->value == 1) {?> checked <?php }?> type="checkbox" value="1">
                    </label>
                </div>
                <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" placeholder="Mot de passe*" type="password" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button class="btn btn-warning" type='reset'>Réinitialiser le formulaire</button>
                        <a href="/ptut/members/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<br>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
