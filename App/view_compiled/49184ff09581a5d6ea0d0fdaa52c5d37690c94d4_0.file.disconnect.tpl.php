<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:49:25
  from "/usr/local/var/www/htdocs/ptut/App/views/connection/disconnect.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4f9b58a9a64_24789587',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49184ff09581a5d6ea0d0fdaa52c5d37690c94d4' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/connection/disconnect.tpl',
      1 => 1490230962,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d4f9b58a9a64_24789587 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="refresh" content="3; URL=/ptut/connection" />
        <title>Projet tut</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
        <?php echo '<script'; ?>
 src="/ptut/Public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/sb-admin-2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/metisMenu.min.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Deconnexion</h3>
                        </div>
                        <div class="panel-body">
                            <p>Vous avez été déconnecté</p>
                            <p>Vous allez être redirigé vers la page de connection dans 3 secondes.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php }
}
