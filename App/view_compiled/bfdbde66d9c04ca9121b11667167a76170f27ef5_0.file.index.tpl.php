<?php
/* Smarty version 3.1.30, created on 2017-03-24 01:54:40
  from "C:\wamp64\www\ptut\App\views\groups\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d47c60d4bfe7_48371857',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bfdbde66d9c04ca9121b11667167a76170f27ef5' => 
    array (
      0 => 'C:\\wamp64\\www\\ptut\\App\\views\\groups\\index.tpl',
      1 => 1490316610,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d47c60d4bfe7_48371857 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
<form style="display: none;" id="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
" role="form" method="post" action="/ptut/groups/edit">
    <input type="hidden" name="ptu_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"> 
    <input type="hidden" name="group_name" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_nom;?>
"> 
    <input type="hidden" name="gro_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
"> 
</form>
<form style="display: none;" id="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
" role="form" method="post" action="/ptut/groups/remove">
    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
"> 
</form>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gestion des groupes
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>                
                        <th class="text-center">Nom du groupe</th>
                        <th class="text-center">Numéro du groupe</th>
                        <th class="text-center">Projet (n°/nom)</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
                    <tr>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->gro_nom;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
 - <?php echo $_smarty_tpl->tpl_vars['ptuts_grp']->value[$_smarty_tpl->tpl_vars['it']->value->ptu_num]->ptu_nom;?>
</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
.submit();">
                                        <a href="#">Editer</a>
                                    </li>
                                    <li onclick="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->gro_num;?>
.submit();">
                                        <a href="#"><span class="glyphicon glyphicon-warning-sign"></span> Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>       
                    </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Ajouter un groupe</h3>
            <form role="form" method="post" action="/ptut/groups/add">  
                <div class="form-group">
                    <label>Nom du groupe</label>
                    <input name="group_name" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                <label>Projet tutoré</label>
                    <select name="ptu_num" required="required" class="form-control">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
 - <?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success" name="addGrp">Ajouter</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
