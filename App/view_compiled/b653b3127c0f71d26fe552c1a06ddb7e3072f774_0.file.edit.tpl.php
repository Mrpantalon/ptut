<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:57:13
  from "/usr/local/var/www/htdocs/ptut/App/views/groups/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4fb89465f48_07904261',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b653b3127c0f71d26fe552c1a06ddb7e3072f774' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/groups/edit.tpl',
      1 => 1490352852,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4fb89465f48_07904261 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members_group']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
<form style="display: none;" id="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
" role="form" method="post">
    <input type="hidden" name="member_id" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
"> 
    <input type="hidden" name="remove_member" value="1"> 
    <input name="gro_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['gro_num']->value;?>
">
    <input name="group_name" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['group_name']->value;?>
">
    <input name="ptu_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['ptu_num']->value;?>
">
</form>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editer un groupe
            </h1>
        </div>
    </div>	
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method='post'>
                <input name="apply" type="hidden" value="1">
                <input name="group_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['gro_num']->value;?>
">
                <div class="form-group">
                    <label>Nom du groupe</label>
                    <input name="group_name" placeholder="Nom*" value="<?php echo $_smarty_tpl->tpl_vars['group_name']->value;?>
" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Projet tutoré</label>  
                    <select name="ptu_num" required="required" class="form-control">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
" 
                            <?php if ($_smarty_tpl->tpl_vars['it']->value->ptu_num === $_smarty_tpl->tpl_vars['ptu_num']->value) {?> 
                            selected 
                            <?php }?>
                            ><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
 - <?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                        <a href="/ptut/groups/index" class="btn btn-danger" role="button" name="Annuler">Annuler</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Prénom</th>
                        <th class="text-center">Mail</th>
                        <th class="text-center">Login</th>  
                        <th class="text-center"></th>                                 
                    </tr>
                </thead>
                <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members_group']->value, 'it2', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it2']->value) {
?>
                    <tr>                                    
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_id;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_nom;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_prenom;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_mail;?>
</td>
                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_login;?>
</td> 
                        <td class="text-center">
                            <button type="button" class="btn btn-danger "onclick="remove_form<?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_id;?>
.submit();">
                                Supprimer
                            </button>
                        </td>                                                   
                    </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form role="form" method='post'>
                <input name="add_member" type="hidden" value="1">
                <input name="gro_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['gro_num']->value;?>
">
                <input name="group_name" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['group_name']->value;?>
">
                <input name="ptu_num" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['ptu_num']->value;?>
">
                <div class="form-group">
                    <label>Ajouté un membre</label>
                    <select name="member_id" required="required" class="form-control">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
" ><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
 - <?php echo $_smarty_tpl->tpl_vars['it']->value->mem_nom;?>
 - <?php echo $_smarty_tpl->tpl_vars['it']->value->mem_prenom;?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </div>
                <button type="submit" class="btn btn-success">Ajouter</button>
            </form>
        </div>
    </div>
<br>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
