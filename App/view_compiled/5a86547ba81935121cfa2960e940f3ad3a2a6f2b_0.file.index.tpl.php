<?php
/* Smarty version 3.1.30, created on 2017-03-24 12:09:10
  from "/usr/local/var/www/htdocs/ptut/App/views/members/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d50c667b3316_74026907',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a86547ba81935121cfa2960e940f3ad3a2a6f2b' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/members/index.tpl',
      1 => 1490355029,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d50c667b3316_74026907 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php if (isset($_smarty_tpl->tpl_vars['err_login']->value) && $_smarty_tpl->tpl_vars['err_login']->value == 1) {?>
        <div class="alert alert-danger alert-dismissable" style="margin-top: 70px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Erreur: cet identifiant de connection existe déja !
        </div>
    <?php }?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
        <form style="display: none;" id="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
" role="form" method="post" action="/ptut/members/edit">
            <input type="hidden" name="member_id" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
"> 
        </form>
        <form style="display: none;" id="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
" role="form" method="post" action="/ptut/members/remove">
            <input type="hidden" name="member_id" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
"> 
        </form>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Membres
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Prénom</th>
                        <th class="text-center">Mail</th>
                        <th class="text-center">Statut</th>
                        <th class="text-center">Identifiant de connexion</th>                                  
                        <th class="text-center">Chef de projet</th>
                        <th class="text-center">Etudiant</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
                    <tr>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
</td>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_nom;?>
</td>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_prenom;?>
</td>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_mail;?>
</td>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_statut;?>
</td>
                        <td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_login;?>
</td>                                 
                        <td class="text-center">
                            <?php if ($_smarty_tpl->tpl_vars['it']->value->mem_chef_projet == 0) {?>
                                non
                            <?php } else { ?>
                                oui
                            <?php }?>
                        </td>
                        <td class="text-center">
                            <?php if ($_smarty_tpl->tpl_vars['it']->value->mem_etudiant == 0) {?>
                                non
                            <?php } else { ?>
                                oui
                            <?php }?>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
.submit();">
                                        <a href="#">Editer</a>
                                    </li>
                                    <li onclick="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
.submit();">
                                        <a href="#"><span class="glyphicon glyphicon-alert"></span> Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </tbody>
            </table>
    	</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>Ajouter un membre</h3>
            <form role="form" method="post" action="/ptut/members/add">  
                <div class="form-group">
                    <label>Nom</label>
                    <input name="member_name" placeholder="Nom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Prénom</label>
                    <input name="member_firstname" placeholder="Prénom*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" placeholder="Email*" type="mail" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Statut</label>
                    <select class="form-control" name="member_status">
                        <option>enseignant</option>
                        <option>étudiant</option>
                        <option>extérieur</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Identifiant de connexion</label>
                    <input name="member_login" placeholder="Identifiant*" type="text" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>
                        Chef de projet <input name="member_projet_owner" type="checkbox"  value="1">
                    </label>
                </div>
                <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" placeholder="Mot de passe*" type="password" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Ajouter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
