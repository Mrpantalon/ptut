<?php
/* Smarty version 3.1.30, created on 2017-03-24 00:51:58
  from "C:\wamp64\www\ptut\App\views\common\404.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d46dae242476_07026796',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c77d2c20bb1dd5e6508bf0fb5c903df4ee960ee8' => 
    array (
      0 => 'C:\\wamp64\\www\\ptut\\App\\views\\common\\404.tpl',
      1 => 1490301912,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./header.tpl' => 1,
    'file:./footer.tpl' => 1,
  ),
),false)) {
function content_58d46dae242476_07026796 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:./header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Erreur 404 !
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
            <?php if ($_smarty_tpl->tpl_vars['invalid_page']->value === 'true') {?>
                <p>Cette page n'existe pas !</p>
            <?php } else { ?>
                <?php if ($_smarty_tpl->tpl_vars['error_action']->value === 'true') {?>
                    <p><?php echo $_smarty_tpl->tpl_vars['action']->value;?>
 n'est pas une action valide sur la page <?php echo $_smarty_tpl->tpl_vars['source']->value;?>
 !</p>
                <?php } else { ?>
                    <p><?php echo $_smarty_tpl->tpl_vars['source']->value;?>
 n'est pas une page valide !</p>
                <?php }?>
            <?php }?>
    	</div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:./footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
