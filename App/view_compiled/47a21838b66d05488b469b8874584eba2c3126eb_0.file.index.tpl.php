<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:49:28
  from "/usr/local/var/www/htdocs/ptut/App/views/connection/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4f9b8cc1a28_31712905',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47a21838b66d05488b469b8874584eba2c3126eb' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/connection/index.tpl',
      1 => 1490292616,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d4f9b8cc1a28_31712905 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Projet tut</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
        <link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
        <?php echo '<script'; ?>
 src="/ptut/Public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/sb-admin-2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ptut/Public/js/metisMenu.min.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Connection</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="/ptut/connection/connect">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Identifiant de connection" name="username" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="rememberme">Se souvenir de moi
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-success btn-block">Se connecter</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php }
}
