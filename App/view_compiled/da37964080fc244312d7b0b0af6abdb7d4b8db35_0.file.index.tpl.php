<?php
/* Smarty version 3.1.30, created on 2017-03-24 11:14:31
  from "/usr/local/var/www/htdocs/ptut/App/views/supervisors/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4ff97e9e0a0_70825823',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'da37964080fc244312d7b0b0af6abdb7d4b8db35' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/supervisors/index.tpl',
      1 => 1490354057,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4ff97e9e0a0_70825823 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Ajouter un Encadrent
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">  
            <form role="form" method="post" action="/ptut/supervisor/add">                
                <div class="form-group">
                    <label>Membre</label>
                    <select name="memberid" required="required" class="form-control">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members']->value, 'it', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id;?>
 - <?php echo $_smarty_tpl->tpl_vars['it']->value->mem_nom;?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </select>
                </div>
                <div class="form-group">
                    <label>Rôle</label>
                    <select name="role" required="required" class="form-control">
                        <option value="commanditaire">commanditaire</option>
                        <option value="tuteur">tuteur</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success">Ajouter</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>

    	</div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
