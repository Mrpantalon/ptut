<?php
/* Smarty version 3.1.30, created on 2017-03-24 01:01:53
  from "/usr/local/var/www/htdocs/ptut/App/views/option/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d47001631506_84580129',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1cc784502ece7204f486f7d68023d90f5da9f676' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/option/index.tpl',
      1 => 1490317309,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d47001631506_84580129 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php if (isset($_smarty_tpl->tpl_vars['success']->value) && $_smarty_tpl->tpl_vars['success']->value == 1) {?>
        <div class="alert alert-success alert-dismissable" style="margin-top: 70px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Les modifications ont bien été prise en compte.
        </div>
    <?php }?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Options de l'utilisateur
            </h1>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">
            <h3>Informations</h3>
            <h4>Nom</h4>
            <p><?php echo $_smarty_tpl->tpl_vars['member']->value->mem_nom;?>
</p>
            <h4>Prénom</h4>
            <p><?php echo $_smarty_tpl->tpl_vars['member']->value->mem_prenom;?>
</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Modifier les informations</h3>
            <form role="form" method="post">  
                <input name="apply" value="1" type="hidden" required="required">
                <div class="form-group">
                    <label>Email</label>
                    <input name="member_mail" value="<?php echo $_smarty_tpl->tpl_vars['member']->value->mem_mail;?>
" placeholder="mail*" type="email" class="form-control" required="required">
                </div>
                 <div class="form-group">
                    <label>Mot de passe</label>
                    <input name="member_password" value="" placeholder="password" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-success" name="addGrp">Modifier</button>
                        <button type="reset" class="btn btn-warning">Reinitialiser le formulaire</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
