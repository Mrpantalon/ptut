<?php
/* Smarty version 3.1.30, created on 2017-03-24 11:57:36
  from "/usr/local/var/www/htdocs/ptut/App/views/meetings/index_admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d509b05ff038_13309130',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef45ca44271c108b9ed616cbbd595a66042c8d6c' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/meetings/index_admin.tpl',
      1 => 1490356526,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d509b05ff038_13309130 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mettings']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
        <form style="display: none;" id="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->reu_num;?>
" role="form" method="post" action="/ptut/meetings/remove">
            <input type="hidden" name="reu_id" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->reu_num;?>
"> 
        </form>
   <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mes réunions
            </h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
    		<?php if (count($_smarty_tpl->tpl_vars['mettings']->value) >= 1) {?>
		        <div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center">Date</th>
								<th class="text-center">Salle</th>
								<th class="text-center">Terminé</th>
								<th class="text-center">Demandeur</th>
								<th class="text-center">Priorité</th>
								<th class="text-center">PTUT</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['mettings']->value)) {?>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mettings']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
								<tr <?php if ($_smarty_tpl->tpl_vars['it']->value->reu_terminer == 1) {?> class='danger' <?php } elseif ($_smarty_tpl->tpl_vars['it']->value->reu_terminer == 2) {?>  class='warning' <?php } else { ?> class='success' <?php }?>>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->reu_dateheure;?>
</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->reu_salle;?>
</td>
									<td class="text-center">
										<?php if ($_smarty_tpl->tpl_vars['it']->value->reu_terminer == 0) {?>
											non
										<?php } else { ?>
											oui
										<?php }?>
									</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id_demand;?>
</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id_priorit;?>
</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['ptuts']->value[$_smarty_tpl->tpl_vars['it']->value->reu_num]->ptu_nom;?>
</td>
									<td class="text-center"><button type="button" class="btn btn-danger" onclick="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->reu_num;?>
.submit();">Supprimer</button></td>
								</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							<?php } else { ?>
								<p>Vous n'avez aucune réunion !</p>
							<?php }?>
						</tbody>
					</table>
				</div>
			<?php } else { ?>
			<p>Vous n'avez aucune réunions !
			<?php }?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<form role="form">
				<div class="form-group">
					<label>Salle</label>
					<input name="room" placeholder="Salle*" type="text"  placeholder="" class="form-control" required="true">
				</div>
				<div class="form-group">
					<label>PTUT</label>
					<select name="applicant" id="ptut" required="true" class="form-control">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts_unique']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
	                		<option value='<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
'><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</option>
	                	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	            	</select>
					</div>
					<div class="form-group">
						<label>Demandeur</label>
						<select name="applicant" id="owner" required="true" class="form-control">
							
		            	</select>
					</div>
				<div class="form-group">
					<label>Priorité</label>
					<select name="priority" id="owner" required="true" class="form-control">
	                    
	                </select>
				</div>
				<div class="form-group">
					<label>Date</label>
					<div class='input-group date' id='datetimepicker'>
	            		<input name="date" type="text" required="true" class="form-control" />
	            		<span class="input-group-addon">
	                	<span class="glyphicon glyphicon-time"></span>
	            		</span>
	        		</div>
	    		</div>
				<div class="form-group">
					<label>PTUT</label>
				</div>
				<div class="form-group">
					<div class="btn-group" role="group" aria-label="...">
							<button class="btn btn-success" type='submit' formmethod='post'>Ajouter</button>
					</div>
				</div>
				<?php echo '<script'; ?>
 type="text/javascript">
					$(function () {
	    				$('#datetimepicker').datetimepicker({
	    					locale: 'fr',
	    					format: "YYYY-MM-DD HH-mm",
	    					sideBySide: true,
	    				});
					});
				<?php echo '</script'; ?>
>
			</form>
		</div>
	</div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
