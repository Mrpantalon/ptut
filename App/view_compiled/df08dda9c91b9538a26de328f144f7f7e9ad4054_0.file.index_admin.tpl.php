<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:43:06
  from "C:\wamp64\www\ptut\App\views\ptuts\index_admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4f83a02f035_99791033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'df08dda9c91b9538a26de328f144f7f7e9ad4054' => 
    array (
      0 => 'C:\\wamp64\\www\\ptut\\App\\views\\ptuts\\index_admin.tpl',
      1 => 1490352028,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4f83a02f035_99791033 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts']->value, 'it', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it']->value) {
?>	
<form style="display: none;" id="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
" role="form" method="post" action="/ptut/ptuts/remove">
	<input type="hidden" name="ptut_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"> 
</form>
<form style="display: none;" id="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
" role="form" method="post" action="/ptut/ptuts/edit">
	<input type="hidden" name="ptut_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"> 
</form>
<form style="display: none;" id="addsupervisor_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
" role="form" method="post" action="/ptut/supervisors/add">
	<input type="hidden" name="ptut_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"> 
</form>
<form style="display: none;" id="removesupervisor_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
" role="form" method="post" action="/ptut/supervisors/remove">
	<input type="hidden" name="ptut_num" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
"> 
</form>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gestion des Projets Tutorés
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th class="text-center">Nom</th>
						<th class="text-center">Numéro</th>					
						<th class="text-center">Description</th>
						<th class="text-center">Date de création</th>
						<th class="text-center">Dossier</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody>	
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts']->value, 'it', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it']->value) {
?>						
					<tr>
						<td class="text-center">
							<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</a>
						</td>
						<td class="text-center">
							<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>

						</td>					
						<td class="text-center">
							<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_description;?>

						</td>
						<td class="text-center">
							<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_date_creation;?>

						</td>
						<td class="text-center">
							<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_dossier_racine;?>

						</td>
						<td class="text-center">
	    					<div class="btn-group">
	    						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li onclick="edit_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
.submit();">
                                    	<a href="#">Editer</a>
                                    </li>
                                    <li onclick="addsupervisor_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
.submit();">
                                    	<a href="#">Ajouter un encadrent</a>
                                    </li>
                                    <li onclick="removesupervisor_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
.submit();">
                                    	<a href="#">Supprimer un encadrent</a>
                                    </li>
                                    <li onclick="remove_form<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
.submit();">
                                    	<a href="#"><span class="glyphicon glyphicon-warning-sign"></span> Supprimer</a>
                                    </li>
                                </ul>
	    					</div>
						</td>					
					</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
    	<div class="col-lg-12">
			<h3>Ajouter un Projet Tutoré</h3>

			<form role="form" method="post" action="/ptut/ptuts/add">
				<div class="form-group">
					<label>Nom du projet</label>
					<input name="name" type="text"  placeholder="Nom*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label>Description</label>
					<input name="description" type="text"  placeholder="Description*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label>Nom dossier</label>
					<input name="directory" type="text"  placeholder="Dossier Racine*" class="form-control" required="required">
				</div>
				<div class="form-group">
					<div class="btn-group" role="group" aria-label="...">
						<button type="submit" class="btn btn-success">Ajouter</button>
					</div>
				</div>
			</form>
    	</div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
