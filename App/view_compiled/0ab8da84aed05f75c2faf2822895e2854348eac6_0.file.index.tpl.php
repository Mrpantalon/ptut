<?php
/* Smarty version 3.1.30, created on 2017-03-24 01:54:15
  from "C:\wamp64\www\ptut\App\views\meetings\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d47c4737b418_94509806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0ab8da84aed05f75c2faf2822895e2854348eac6' => 
    array (
      0 => 'C:\\wamp64\\www\\ptut\\App\\views\\meetings\\index.tpl',
      1 => 1490316706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d47c4737b418_94509806 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mes réunions
            </h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
		        <div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center">Date</th>
								<th class="text-center">Salle</th>
								<th class="text-center">Terminé</th>
								<th class="text-center">Demandeur</th>
								<th class="text-center">Priorité</th>
								<th class="text-center">PTUT</th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['mettings']->value)) {?>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mettings']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
								<tr <?php if ($_smarty_tpl->tpl_vars['it']->value->reu_terminer == 1) {?> class='danger' <?php }?>>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->reu_dateheure;?>
</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->reu_salle;?>
</td>
									<td class="text-center">
										<?php if ($_smarty_tpl->tpl_vars['it']->value->reu_terminer == 0) {?>
											non
										<?php } else { ?>
											oui
										<?php }?>
									</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id_demand;?>
</td>
									<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->mem_id_priorit;?>
</td>
									<td class="text-center"><a href="#" role="button"><?php echo $_smarty_tpl->tpl_vars['ptuts']->value[$_smarty_tpl->tpl_vars['it']->value->reu_num]->ptu_nom;?>
</a></td>
								</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							<?php } else { ?>
								<p>Vous n'avez aucune réunion !</p>
							<?php }?>
						</tbody>
					</table>
				</div>
		</div>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['member']->value->mem_chef_projet == 1 || $_smarty_tpl->tpl_vars['member']->value->mem_statut === 'enseignant' || $_smarty_tpl->tpl_vars['member']->value->mem_statut === 'extérieur') {?>
	<div class="row">
		<div class="col-lg-12">
			<form role="form">
				<div class="form-group">
					<label>Salle</label>
					<input name="room" placeholder="Salle≈*" type="text"  placeholder="" class="form-control" required="true">
				</div>
				<?php if ($_smarty_tpl->tpl_vars['member']->value->mem_statut === 'enseignant' || $_smarty_tpl->tpl_vars['member']->value->mem_statut === 'extérieur') {?>
					<div class="form-group">
						<label>PTUT</label>
						<select name="applicant" id="ptut" required="true" class="form-control">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptuts_unique']->value, 'it');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['it']->value) {
?>
		                		<option value='<?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
'><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</option>
		                	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		            	</select>
					</div>
				<?php } else { ?>
					<input name="applicant" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['member']->value->mem_id;?>
" class="form-control" required="true">
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['member']->value->mem_statut === 'enseignant' || $_smarty_tpl->tpl_vars['member']->value->mem_statut === 'extérieur') {?>
					<div class="form-group">
						<label>Demandeur</label>
						<select name="applicant" id="owner" required="true" class="form-control">
							<optgroup class="active" id="toto">

							</optgroup>
		            	</select>
					</div>
				<?php } else { ?>
					<input name="applicant" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['member']->value->mem_id;?>
" class="form-control" required="true">
				<?php }?>
				<div class="form-group">
					<label>Priorité</label>
					<select name="priority" id="owner" required="true" class="form-control">
	                    
	                </select>
				</div>
				<div class="form-group">
					<label>Date</label>
					<div class='input-group date' id='datetimepicker'>
	            		<input name="date" type="text" required="true" class="form-control" />
	            		<span class="input-group-addon">
	                	<i class="fa fa-calendar"></i>
	            		</span>
	        		</div>
	    		</div>
				<div class="form-group">
					<label>PTUT</label>
				</div>
				<div class="form-group">
					<div class="btn-group" role="group" aria-label="...">
							<button class="btn btn-success" type='submit' formmethod='post'>Ajouter</button>
							<button class="btn btn-warning" type='reset'>Réinitialiser le formulaire</button>
					</div>
				</div>
				<?php echo '<script'; ?>
 type="text/javascript">
					$(function () {
	    				$('#datetimepicker').datetimepicker({
	    					locale: 'fr',
	    					format: "YYYY-MM-DD HH-mm",
	    					sideBySide: true,
	    					icons: {
			                    time: "fa fa-clock-o",
			                    date: "fa fa-calendar",
			                    up: "fa fa-arrow-up",
			                    down: "fa fa-arrow-down"
			                }
	    				});
					});
				<?php echo '</script'; ?>
>
			</form>
		</div>
	</div>
	<?php }
$_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
