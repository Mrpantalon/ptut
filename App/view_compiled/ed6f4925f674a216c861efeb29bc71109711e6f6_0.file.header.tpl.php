<?php
/* Smarty version 3.1.30, created on 2017-03-24 00:51:57
  from "C:\wamp64\www\ptut\App\views\common\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d46daddceb79_94324533',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ed6f4925f674a216c861efeb29bc71109711e6f6' => 
    array (
      0 => 'C:\\wamp64\\www\\ptut\\App\\views\\common\\header.tpl',
      1 => 1490316383,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d46daddceb79_94324533 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Projet tut</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/ptut/Public/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/bootstrap-datetimepicker.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/sb-admin-2.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/metisMenu.min.css" type="text/css">
		<link rel="stylesheet" href="/ptut/Public/css/moment-with-locales.min.css" type="text/css">
		<?php echo '<script'; ?>
 src="/ptut/Public/js/jquery.min.js"><?php echo '</script'; ?>
>
    	<?php echo '<script'; ?>
 src="/ptut/Public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    	<?php echo '<script'; ?>
 src="/ptut/Public/js/sb-admin-2.min.js"><?php echo '</script'; ?>
>
    	<?php echo '<script'; ?>
 src="/ptut/Public/js/metisMenu.min.js"><?php echo '</script'; ?>
>

    	<style>
    		body, html {
			  height: 100%;
			}
			.fill { 
				min-height: 100%;
				height: 100%;
			}
			@-moz-document url-prefix() {
			  select.form-control {
			    padding-right: 25px;
			    background-image: url("data:image/svg+xml,\
			      <svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='14px'\
			           height='14px' viewBox='0 0 1200 1000' fill='rgb(51,51,51)'>\
			        <path d='M1100 411l-198 -199l-353 353l-353 -353l-197 199l551 551z'/>\
			      </svg>");
			    background-repeat: no-repeat;
			    background-position: calc(100% - 7px) 50%;
			    -moz-appearance: none;
			    appearance: none;
			  }
			}
    	</style>
    	<!--<?php echo '<script'; ?>
 src="Public/js/moment.min.js"><?php echo '</script'; ?>
>-->
    	<!--<?php echo '<script'; ?>
 src="Public/js/bootstrap-datetimepicker.js"><?php echo '</script'; ?>
>-->

	    <!--[if lt IE 9]>
	        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
	        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
	    <![endif]-->
	</head>
	<body>
		<div id="wrapper" class="fill">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="/ptut/index">Gestion Projet tutoré</a>
	            </div>

	            <ul class="nav navbar-top-links navbar-right">
		            <li class="dropdown">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #eee" onmouseover="this.style.backgroundColor='#111'" onmouseout="this.style.backgroundColor='#222'">
	                    	<span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
 <b class="caret"></b>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <a href="/ptut/options">Options du profil</a>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <a href="/ptut/connection/disconnect">Déconnexion</a>
	                        </li>
	                    </ul>
	                </li>
	            </ul>

	            <div class="navbar-default sidebar" role="navigation">
	            	<div class="sidebar-nav navbar-collapse collapse">
                		<ul class="nav" id="side-menu">
                			<li><a href="/ptut/index">Acceuil</a></li> 		
                			<li><a href="/ptut/ptuts">Projets tutoré<?php if ($_smarty_tpl->tpl_vars['admin']->value === true) {?>s<?php }?></a></li> 
                			<?php if ($_smarty_tpl->tpl_vars['admin']->value === true) {?>								
								<li><a href="/ptut/groups">Groupes</a></li> 
								<li><a href="/ptut/members">Membres</a></li> 
							<?php }?>
							<li><a href="/ptut/meetings">Réunions</a></li>
                		</ul>
                	</div>
	            </div>
			</nav>
			<div id="page-wrapper" class="fill" style="margin-top: 30px">
				<div class="fill">
<?php }
}
