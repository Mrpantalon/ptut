<?php
/* Smarty version 3.1.30, created on 2017-03-24 10:49:37
  from "/usr/local/var/www/htdocs/ptut/App/views/ptuts/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58d4f9c1a4ea26_95391406',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8662b28c305b0f9b94310c988a655484d2a7552a' => 
    array (
      0 => '/usr/local/var/www/htdocs/ptut/App/views/ptuts/index.tpl',
      1 => 1490352558,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../common/header.tpl' => 1,
    'file:../common/footer.tpl' => 1,
  ),
),false)) {
function content_58d4f9c1a4ea26_95391406 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="pull-right">
	    					<div class="btn-group">
	    						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                    	<a href="#">Editer le PTUT</a>
                                    </li>
                                    <li>
                                    	<a href="#">Ajouter un encadrent</a>
                                    </li>
                                    <li>
                                    	<a href="#">Supprimer un encadrent</a>
                                    </li>
                                </ul>
	    					</div>
	    				</div>

<?php $_smarty_tpl->_subTemplateRender("file:../common/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mon Projet Tutoré
            </h1>
        </div>
    </div>	
    <div class="row">
    	<div class="col-lg-12">
    		<?php if (count($_smarty_tpl->tpl_vars['ptut']->value) >= 1) {?>
	    		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ptut']->value, 'it', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it']->value) {
?>
	    		<?php if (count($_smarty_tpl->tpl_vars['ptut']->value) >= 2) {?>
		    		<div class="panel panel-default">
		    			<div class="panel-heading">
		    				<h3 class="panel-title">Projet tutoré: <?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</h3>
		    			</div>
		    			<div class="panel-body">
	    		<?php }?>
		    			<div class="table-responsive">	    					
					    	<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
									<?php if (count($_smarty_tpl->tpl_vars['ptut']->value) === 1) {?>
										<th class="text-center">Nom</th>
									<?php }?>
										<th class="text-center">Numéro</th>					
										<th class="text-center">Description</th>
										<th class="text-center">Date de création</th>
										<th class="text-center">Dossier racine</th>
									</tr>
								</thead>
								<tbody>								
									<tr>
									<?php if (count($_smarty_tpl->tpl_vars['ptut']->value) === 1) {?>
										<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_nom;?>
</a></td>
									<?php }?>
										<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_num;?>
</td>					
										<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_description;?>
</td>
										<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_date_creation;?>
</td>
										<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it']->value->ptu_dossier_racine;?>
</td>
									</tr>
								</tbody>
							</table>

							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Nom</th>
										<th class="text-center">Prénom</th>
										<th class="text-center">Mail</th>
										<th class="text-center">Statut</th>
										<th class="text-center">Login</th>									
										<th class="text-center">Chef de projet</th>
										<th class="text-center">Etudiant</th>
									</tr>
								</thead>
								<tbody>
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['members']->value[$_smarty_tpl->tpl_vars['it']->value->ptu_num], 'it2', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['it2']->value) {
?>
									<tr>									
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_id;?>
</td>
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_nom;?>
</td>
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_prenom;?>
</td>
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_mail;?>
</td>
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_statut;?>
</td>
										<td td class="text-center"><?php echo $_smarty_tpl->tpl_vars['it2']->value->mem_login;?>
</td>									
										<td class="text-center">
										<?php if ($_smarty_tpl->tpl_vars['it2']->value->mem_chef_projet == 0) {?>
											non
										<?php } else { ?>
											oui
										<?php }?>
										</td>
										<td class="text-center">
										<?php if ($_smarty_tpl->tpl_vars['it2']->value->mem_etudiant == 0) {?>
											non
										<?php } else { ?>
											oui
										<?php }?>
										</td>								
									</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

									</tbody>
							</table>	
						</div>									
					<?php if (count($_smarty_tpl->tpl_vars['ptut']->value) >= 2) {?>
	                </div>
	            </div>
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>
						
			<?php } else { ?>
				<p>Vous n'êtes dans aucun PTUT !</p>
			<?php }?>
    	</div>
    </div>
<?php $_smarty_tpl->_subTemplateRender("file:../common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
